<?php

function perfectmoney_sci_prefillform($form_state){
   
	$form=array(); $currencies_ar=array();

	$currency_settings = unserialize(variable_get('perfectmoney_sci_currencies', serialize(_perfectmoney_sci_GetDefCurSetts())));
	foreach($currency_settings as $key=>$value){
		if($value['enabled']==1) $currencies_ar[$key]=$key;
	}

	
	$form['currency'] = array(
		'#type' => 'radios',
		'#title' => t("Currency"),
		'#options' => $currencies_ar,
		'#default_value' => 'USD',
		//'#description' => t("Select payment currency."),
		'#required' => TRUE
	);
 

	$form['amount'] = array(
		'#type' => 'textfield', 
		'#title' => t('Amount'), 
		'#default_value' => '', 
		'#size' => 10, 
		'#maxlength' => 12, 
		'#required' => TRUE
	);

	$form['memo'] = array(
		'#type' => 'textarea', 
		'#title' => t('Memo'), 
		'#default_value' => t('Payment to !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))), 
		'#description' => t("Payment description."),
		'#required' => TRUE
	);


	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Cerate payment'),
	);

	return $form;

}

function perfectmoney_sci_prefillform_submit(&$form, $form_state){
	
	$payment=_perfectmoney_sci_createpayment(array(
		'amount'=>$form_state['values']['amount'],
		'currency'=>$form_state['values']['currency'],
		'memo'=>$form_state['values']['memo'],
	));


	if(is_array($payment) && $payment['pid']>0){
		drupal_set_message("Please confirm payment details");
		drupal_goto('perfectmoney/payment/'.$payment['pid']);
	}

}

function perfectmoney_sci_sciform($form_state, $payment){

	global $base_url;

	if(!is_array($payment) && is_integer($payment)){ // fetch payment info from DB
		$payment=perfectmoney_sci_pid_load($payment);
	}

	if(!is_array($payment) || !($payment['pid']>0)){

		$form['error'] = array(
		  '#type' => 'item',
		  '#title' => t('Error'),
		  '#value' => t('Order you are going to pay for does not exist'),
		);

	}else{

		$form['#action'] = variable_get('perfectmoney_sci_payment_url', PERFECTMONEY_SCI_PAYMENT_URL);
		
		$currency_settings = unserialize(variable_get('perfectmoney_sci_currencies', serialize(_perfectmoney_sci_GetDefCurSetts())));
		$presc = $currency_settings["{$payment['currency']}"]['presc'];

		// interface data:

		$form['payment_id'] = array(
		  '#type' => 'item',
		  '#title' => t('Order #'),
		  '#value' => $payment['pid'],
		);

		$form['amount'] = array(
		  '#type' => 'item',
		  '#title' => t('Amount'),
		  '#value' => round($payment['amount'], $presc).' '.$payment['currency'],
		);

		$form['memo'] = array(
		  '#type' => 'item',
		  '#title' => t('Memo'),
		  '#value' => $payment['memo']
		);

		// SCI data...

		$form['PAYEE_ACCOUNT'] = array(
			'#type' => 'hidden',
			'#value' => $payment['payee_account'],
		);

		$form['PAYMENT_ID'] = array(
			'#type' => 'hidden',
			'#value' => $payment['pid'],
		);
	 
		$form['SUGGESTED_MEMO'] = array(
			'#type' => 'hidden',
			'#value' => $payment['memo'],
		);

		$form['PAYMENT_AMOUNT'] = array(
			'#type' => 'hidden',
			'#value' => round($payment['amount'], $presc),
		);

		$form['PAYMENT_UNITS'] = array(
			'#type' => 'hidden',
			'#value' => $payment['currency'],
		);

		$form['PAYEE_NAME'] = array(
			'#type' => 'hidden',
			'#value' => variable_get('perfectmoney_sci_payee_name',  variable_get('site_name', 'Drupal')),
		);

		$form['STATUS_URL'] = array(
			'#type' => 'hidden',
			'#value' => $base_url.'/'.drupal_get_path_alias('perfectmoney/status'),
		);

		$form['PAYMENT_URL'] = array(
			'#type' => 'hidden',
			'#value' => $base_url.'/perfectmoney/success',
		);

		$form['PAYMENT_URL_METHOD'] = array(
			'#type' => 'hidden',
			'#value' => 'LINK',
		);

		$form['NOPAYMENT_URL'] = array(
			'#type' => 'hidden',
			'#value' => $base_url.'/perfectmoney/fail',
		);

		$form['NOPAYMENT_URL_METHOD'] = array(
			'#type' => 'hidden',
			'#value' => 'LINK',
		);

		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Pay now'),
		);

	}

	return $form;

}

function perfectmoney_sci_success(){

	return theme('perfectmoney_sci_success');

}

function perfectmoney_sci_fail(){
	return theme('perfectmoney_sci_fail');
}


function perfectmoney_sci_status(){

	drupal_set_header('Content-type: text/html; charset=iso-8859-1');

	$debug_errors=false;

	// check url
	$url=trim($_SERVER['REQUEST_URI'], '/');
	$alias=drupal_get_path_alias('perfectmoney/status');
	if($url!=$alias) die();

	$created=time();

	$string=
		  $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
		  $_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
		  $_POST['PAYMENT_BATCH_NUM'].':'.
		  $_POST['PAYER_ACCOUNT'].':'.strtoupper(md5(variable_get('perfectmoney_sci_secretkey', ''))).':'.
		  $_POST['TIMESTAMPGMT'];

	$hash=strtoupper(md5($string));

	if(preg_match("/^[0-9]{1,10}$/", $_POST['PAYMENT_ID']) && $hash==$_POST['V2_HASH']){ // proccessing payment if only hash is valid

	   /* In section below you must implement comparing of data you recieved
	   with data you sent. This means to check if $_POST['PAYMENT_AMOUNT'] is
	   particular amount you billed to client and so on. */

	   $payment=perfectmoney_sci_pid_load($_POST['PAYMENT_ID']);
	   if(!is_array($payment)){
	      if($debug_errors){
		     $f=fopen("pmlog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' error: payment not found'."\n");
			 fclose($f);
		  }
		  die();
	   }

	   $currency_settings = unserialize(variable_get('perfectmoney_sci_currencies', serialize(_perfectmoney_sci_GetDefCurSetts())));
	   $presc = $currency_settings["{$payment['currency']}"]['presc'];


	   if($_POST['PAYMENT_AMOUNT']==round($payment['amount'], $presc) && $_POST['PAYEE_ACCOUNT']==$payment['payee_account'] && $_POST['PAYMENT_UNITS']==$payment['currency']){

		  /* ...insert some code to proccess valid payments here... */

		  // enroll payment
		  $result = db_query('UPDATE {perfectmoney_sci} SET batch=%d, payer_account=\'%s\', enrolled=%d WHERE pid=%d', $_POST['PAYMENT_BATCH_NUM'], $_POST['PAYER_ACCOUNT'], $created, $payment['pid']);
					
		  // fire hook
		  $payment['batch']=$_POST['PAYMENT_BATCH_NUM'];
		  $payment['payer_account']=$_POST['PAYER_ACCOUNT'];
		  $payment['enrolled']=$created;
		  module_invoke_all('perfectmoney_sci', 'enrolled', $payment['pid'], $payment);

		  if($debug_errors){
		     $f=fopen("pmlog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' payment OK!'."\n");
			 fclose($f);
		  }

		  die();

	   }else{ // you can also save invalid payments for debug purposes

	      if($debug_errors){
		     $f=fopen("pmlog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' error: fake data'."\n");
			 fclose($f);
		  }
		  die();

	   }


	}else{ // you can also save invalid payments for debug purposes

	   // uncomment code below if you want to log requests with bad hash
	   if($debug_errors){
	      $f=fopen("pmlog.txt", "ab");
		  fwrite($f,date("m/d/Y H:i:s", $created).' error: bad hash or payment_id'."\n");
		  fclose($f);
	   }
	   die();

	}

}